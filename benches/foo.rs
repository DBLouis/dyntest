use criterion::{black_box, criterion_group, criterion_main, Criterion};

trait Foo {
    fn foo(&self, v: f64) -> f64;
}

struct ImplA;
impl Foo for ImplA {
    fn foo(&self, v: f64) -> f64 {
        v.cos()
    }
}

struct ImplB;
impl Foo for ImplB {
    fn foo(&self, v: f64) -> f64 {
        v.sin()
    }
}

fn call_dyn(t: &dyn Foo, v: f64) {
    t.foo(v);
}

fn call_generic<T: Foo>(t: &T, v: f64) {
    t.foo(v);
}

pub fn criterion_benchmark(c: &mut Criterion) {
    c.bench_function("call_dyn_a", |b| {
        b.iter(|| call_dyn(&ImplA, black_box(2.0)))
    });
    //c.bench_function("call_dyn_b", |b| b.iter(|| {
    //    call_dyn(&ImplB, black_box(2.0))
    //}));
    c.bench_function("call_generic_a", |b| {
        b.iter(|| call_generic(&ImplA, black_box(2.0)))
    });
    //c.bench_function("call_generic_b", |b| b.iter(|| {
    //    call_generic(&ImplB, black_box(2.0))
    //}));
}

criterion_group!(benches, criterion_benchmark);
criterion_main!(benches);
